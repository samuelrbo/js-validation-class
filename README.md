# validation.class.js

This class use pure Javascript to do a form validation. In this version only required and email validations are done. In a near future more validations will be inserted.

##DONE:
[2014-03-25]

  * Class version attribute;
  * Adjust in the hidden inputs to validate, not validating;
  * Adjust in the arguments settings;
  * Adjusted the email validation. Now only process this validation if field is not empty;
  * Adjusted the example in index.html;
  * Include QUnit test;

[2014-03-23]

  * index.html with usage example;
  * Improvements in validation method;
  * Add Email validaition;

# Usage

Call the class in your projects <head>

```html
<script src="validation.class.js"></script>
OR
<script src="validation.class.min.js"></script>
```

## Create
```js
var formValidation = new Validation('formId',{ 
    'htmlElements' : new Array('input'), 
    'messages': {
        required: 'The field {0} is required',
        email: 'The {0} field is not a valid email'
    }
});
```

## Validate
```js
if(!formValidation.validate()) { 
    alert(formValidation.getError()); 
}
```

# License

MIT-License (see LICENSE file).


# Contributing

Your contribution is welcome.


# TODO

  * Provide a live example
  * Class documentation
  * More validation types/methods
