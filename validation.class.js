/**
 * Pure JS Validation Class
 * 
 * Only required method is ready to use, other methods are not implemented yet.
 * 
 * 	var formValidation = new Validation('addCardapioForm',{ 
 * 		'htmlElements' : new Array('input','textarea'), 
 * 		'messages': {
 * 			required: 'The field {0} is required' 
 * 		}
 * 	});
 * 
 * 	if(!formValidation.validate()) { 
 * 		alert(formValidation.getError()); 
 * 	}
 * 
 * @version 1.0.2
 * @since 2014-02-27 20:14
 * @author Samuel Ramon <samuelrbo@gmail.com>
 * @site https://bitbucket.org/samuelrbo/js-validation-class
 * @site https://github.com/samuelrbo/js-validation-class
 * 
 * @param id
 * @param args
 * @returns {Validation}
 */
function Validation(id, args) {
	this.version = '1.0.2';

	/* Class attributes with default values */
	var $this = this,
		form,
		htmlElements = new Array('input','textarea','select','checkbox'),
		specialTypes = new Array('checkbox'),
		excludeHiddens = true,
		requireds = new Array(),
		emails = new Array(),
		errorDelimiters = {
			init: '<p>',
			end: '</p>'
		},
		messages = {
			required: 'The {0} field is required',
			email: 'The {0} field is not a valid email'
		},
		error = '';

	switch(typeof id) {
		case 'string': form = ValidationHelper.get(id);
			break;
		case 'object': form = id;
			break;
		default:
			throw new Error('Invalid Object for validation');
	}

	/* Helper to improve DOM management */
	var ValidationHelper = {
		/* Improvment for document.getElementById() */
		get: function(id) {
			if (typeof id == 'object') {
				return id;
			}
			return document.getElementById(id);
		},
		/* Improvment for getElementsByTagName() using a DOM element as a parent */
		children: function(id, tagName) {
			if (this.get(id))
				return this.get(id).getElementsByTagName(tagName);

			return new Array();
		},
		/* String helper */
		trim: function(str) {
			return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		},
		/* Helper to handle validation's messages */
		formatTxt: function (text) {
			var args = Array.prototype.slice.call(arguments, 1);
		    return text.replace(/{(\d+)}/g, function(match, number) {
		    	return typeof args[number] != 'undefined' ? args[number] : match;
		    });
		},
		/* 
		Helper to get the Label for the DIM element to use in the validation text, 
		if not set the class will use the DOM element name attribute 
		*/
		labelFor: function (element) {
			var labels = this.children(form,'label');
			for( var i = 0; i < labels.length; i++ ) {
				if (labels[i].htmlFor == element.getAttribute('id')){
					return ValidationHelper.trim(labels[i].innerHTML);
				}
			}
		}
	};

	$this.addHtmlElement = function(element) {
		htmlElements.push(element);
	};
	
	$this.getError = function() {
		return error;
	};

	$this.setHtmlElements = function(elements) {
		switch (typeof elements) {
			case 'array':
			case 'object': htmlElements = elements;
				break;
			case 'string': $this.addHtmlElement(elements);
				break;
			default:
				throw new Error('Invalid HTML value');
		}
	};
	
	$this.setMessages = function(msg) {
		if (typeof msg != 'object') {throw new Error('Invalid message object');}
		message = msg;
	};
	
	$this.addMessage = function(type, msg) {
		message[type] = msg;
	};

	$this.setExcludeHiddens = function(yesOrNo) {
		if (typeof yesOrNo != 'boolean') {throw new Error('Invalid boolean value');}
		excludeHiddens = yesOrNo;
	};
	
	$this.setErrorDelimiters = function(delimiters) {
		if (typeof delimiters != 'object') {throw new Error('Invalid delimiters value');}
		errorDelimiters = delimiters;
	};

	$this.getForm = function() {
		return form;
	};
	
	$this.addRequired = function(element) {
		requireds.push(element);
	};

	$this.addEmailValidation = function(element) {
		emails.push(element);
	}

	if (args !== undefined) {
		if (args.htmlElements !== undefined) {$this.setHtmlElements(args.htmlElements);}
		if (args.excludeHiddens !== undefined) {$this.setExcludeHiddens(args.excludeHiddens);}
		if (args.messages !== undefined) {$this.setMessages(args.messages);}
		if (args.errorDelimiters !== undefined) {$this.setErrorDelimiters(args.errorDelimiters);}
	}

	var loadValidations = function() {
		var children, child;
		htmlElements.forEach(function(value,index){
			children = ValidationHelper.children(form, value);
			for(var i = 0; i<children.length; i++) {
				child = children[i];
				if (child.getAttribute('name') != null && child.getAttribute('name') !== '') {
					switch (value) {
						case 'input':
							if (excludeHiddens && child.getAttribute('type') === 'hidden') { continue; }
							if (child.getAttribute('type') === 'radio') {
								throw new Error('HTML element (radio) not supported for this validation class (yet =D)');
							}

							if (child.className.match(/email/i) && ValidationHelper.trim(child.value) !== '') {
								$this.addEmailValidation(child);
							}
						case 'select':
						case 'textarea':
						case 'checkbox':
							if (child.className.match(/required/i)) {
								$this.addRequired(child);
							}
							break;
						default:
							throw new Error('HTML element ('+value+') not supported for this validation class (yet =D)');
					}
				}
			}
		});
	};
	
	loadValidations();

	var addErrorMessage = function(obj, type) {
		error += errorDelimiters.init;
		if (ValidationHelper.labelFor(obj) != null) {
			error += ValidationHelper.formatTxt(messages[type], ValidationHelper.labelFor(obj));
		} else if(obj.dataset.originalTitle != null) {
			error += ValidationHelper.formatTxt(messages[type], obj.dataset.originalTitle);
		} else {
			error += ValidationHelper.formatTxt(messages[type], obj.getAttribute('name'));
		}
		error += errorDelimiters.end;
	}
	
	var requireValidationOk = function() {
		requireds.forEach(function(obj, index){
			if(specialTypes.indexOf(obj.type) === -1 && ValidationHelper.trim(obj.value) === '') {
				addErrorMessage(obj, 'required');
			} else {
				switch (obj.type) {
					case 'checkbox': if (!obj.checked) { addErrorMessage(obj, 'required'); }
						break;
				}
			}
		});
	};

	var emailValidationOk = function() {
		var emailRE = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		emails.forEach(function(obj, index){
    		if(!emailRE.test(obj.value)) {
    			addErrorMessage(obj, 'email');
    		}
		});
	}

	$this.validate = function() {
		requireValidationOk();
		emailValidationOk();
		if (error !== '') { return false; }
		return true;
	};
};