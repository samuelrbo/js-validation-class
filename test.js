function newForm() {
	var testForm = document.createElement('form');
	 	testForm.setAttribute('name','testForm2');
		testForm.setAttribute('id','testForm2');
	return testForm;
}

test('[Tests] Validation Class',function(){
	var options = {
		errorDelimiters: {
		init: '',
		end: ''
	}
	};

	var requiredMessage = '';
	var emailMessage = '';

	var testEmptyForm = newForm();
	var emptyFormValidation = new Validation(testEmptyForm,options);

	/* 1 */
	equal(true, emptyFormValidation.validate(), 'Empty form always return true');

	/* 2 */
	var testInputForm = newForm();
	var inputName = document.createElement('input');
		inputName.setAttribute('type','text');
		inputName.setAttribute('name','name');
		inputName.setAttribute('id','name');
		inputName.className = 'required';
	testInputForm.appendChild(inputName);

	var simpleInputValidation = new Validation(testInputForm,options);
	equal(false, simpleInputValidation.validate(), 'Empty <input:text> with required validation CSS class. [Error Message]: "'+simpleInputValidation.getError()+'"');

	/* 3 */
	var labelName = document.createElement('label');
		labelName.setAttribute('for','name');
		labelName.innerHTML = 'Label Name';
	testInputForm.appendChild(labelName);

	var simpleInputValidationWithLabel = new Validation(testInputForm,options);
	equal(false, simpleInputValidationWithLabel.validate(), 'Empty <input:text> with required validation CSS class with setted Label. [Error Message]: "'+simpleInputValidationWithLabel.getError()+'"');

	/* 4 */
	inputName.value = 'Tester Guy';
	simpleInputValidationWithLabel = new Validation(testInputForm,options);
	equal(true, simpleInputValidationWithLabel.validate(), '<input:text> with required class with a value.');

	/* 5 */
	var testHiddenForm = newForm();
	var inputHidden = document.createElement('input');
		inputHidden.setAttribute('type','hidden');
		inputHidden.setAttribute('name','user');
		inputHidden.setAttribute('id','user');
		inputHidden.className = 'required';
	testHiddenForm.appendChild(inputHidden);

	var inputHiddenExcludedValidation = new Validation(testHiddenForm,options);
	equal(true, inputHiddenExcludedValidation.validate(), 'Empty <input:hidden> with required validation CSS class but Validation Class setted with excludeHiddens:true.');

	/* 6 */
	options.excludeHiddens = false;
	var inputHiddenValidation = new Validation(testHiddenForm,options);
	equal(false, inputHiddenValidation.validate(), 'Empty <input:hidden> with required validation CSS class but Validation Class setted with excludeHiddens:false. [Error Message]: "'+inputHiddenValidation.getError()+'"');

	/* 7 */
	inputHidden.value = '99';
	var inputHiddenValidation = new Validation(testHiddenForm,options);
	equal(true, inputHiddenValidation.validate(), '<input:hidden> with required validation CSS class with Validation Class setted with excludeHiddens:false and a value.');

	/* 8 */
	var testEmailForm = newForm()
	var inputEmail = document.createElement('input');
		inputEmail.setAttribute('type','text');
		inputEmail.setAttribute('name','email');
		inputEmail.setAttribute('id','email');
		inputEmail.className = 'required email';
	testEmailForm.appendChild(inputEmail);

	var emptyEmailValidation = new Validation(testEmailForm,options);
	equal(false, emptyEmailValidation.validate(), 'Empty <input:text> with validation classes, required and email. [Error Message]: "'+emptyEmailValidation.getError()+'"');

	/* 9 */
	var labelEmail = document.createElement('label');
		labelEmail.setAttribute('for','email');
		labelEmail.innerHTML = 'Email';
	testEmailForm.appendChild(labelEmail);

	var emptyEmailValidationWithLabel = new Validation(testEmailForm,options);
	equal(false, emptyEmailValidationWithLabel.validate(), 'Empty <input:text> with validation classes, required and email, with setted Label. [Error Mesasge]: "'+emptyEmailValidationWithLabel.getError()+'"');

	/* 10 */
	inputEmail.value = 'tester.guy@err';
	emptyEmailValidationWithLabel = new Validation(testEmailForm,options);
	equal(false, emptyEmailValidationWithLabel.validate(), '<input:text> with validation classes, required and email, with setted Label and a wrong email. [Error Message]: "'+emptyEmailValidationWithLabel.getError()+'"');

	/* 11 */
	inputEmail.value = 'tester.guy@testergroup.com';
	emptyEmailValidationWithLabel = new Validation(testEmailForm,options);
	equal(true, emptyEmailValidationWithLabel.validate(), '<input:text> with validation classes, required and email, with setted Label and a correct email.');

	/* 12 */
	var testAgreeForm = newForm();
	var inputAgree = document.createElement("input");
		inputAgree.setAttribute("type","checkbox");
		inputAgree.setAttribute("name","agree");
		inputAgree.setAttribute("id","agree");
		inputAgree.value = '1';
		inputAgree.checked = false;
		inputAgree.className = "required";
	testAgreeForm.appendChild(inputAgree);

	var noCheckValidation = new Validation(testAgreeForm,options);
	equal(false, noCheckValidation.validate(), '<input:checkbox> with required validation classe and not checked. [Error Message]: "'+noCheckValidation.getError()+'"');

	/* 13 */
	var labelAgree = document.createElement("label");
		labelAgree.setAttribute("for","agree");
		labelAgree.innerHTML = 'Agreement';
	testAgreeForm.appendChild(labelAgree);

	var noCheckValidationWithLabel = new Validation(testAgreeForm,options);
	equal(false, noCheckValidationWithLabel.validate(), '<input:checkbox> with required validation classe and not checked, with setted Label. [Error Message]: "'+noCheckValidationWithLabel.getError()+'"');

	/* 14 */
	inputAgree.checked = true;
	var checkValidation = new Validation(testAgreeForm,options);
	equal(true, checkValidation.validate(), '<input:checkbox> with required validation classe and checked, with setted Label.');

	/* 15 */
	var testForm2 = newForm();
		testForm2.appendChild(labelName);
		testForm2.appendChild(inputName);

		testForm2.appendChild(inputHidden);

		testForm2.appendChild(labelEmail);
		testForm2.appendChild(inputEmail);

		testForm2.appendChild(inputAgree);
		testForm2.appendChild(labelAgree);

	var testValidation = new Validation(testForm2,options);
	equal(true, checkValidation.validate(), 'All validation OK');
});